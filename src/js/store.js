import axios from 'axios'

export default {
  state: {
    productsData   : [],
    checkedFilters : {
      brand      : [],
      promotions : [],
    },
  },

  actions: {
    fetchProducts(context) {
      axios.get('/products.json')
        .then(resp => {
          context.commit('setProductData', resp.data)

          //Load images
          resp.data.forEach((product, index) => {
            const img = new Image()
            img.onload = () => context.commit('replacePlaceholder', index)
            img.src = product.image
          })
        })
        .catch(resp => {
          //todo show an error
          console.error(resp.data)
        })
    },
  },

  mutations: {
    /**
     * Set productsData
     * @param  {object} state
     * @param  {array} sentProductData
     */
    setProductData(state, sentProductData = []) {
      state.productsData = sentProductData
    },

    /**
     * Reset sent filter array
     * @param  {object} state
     * @param  {string} sentFilter
     */
    resetFilterGroup(state, sentFilter) {
      state.checkedFilters[sentFilter].length = 0
    },

    /**
     * Replace placeholder image with real one
     * @param  {object} context
     * @param  {string} index
     */
    replacePlaceholder(state, index) {
      state.productsData[index].placeholder = state.productsData[index].image
    },
  },
}
